using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JondellCorp.Models
{
	public class Statement
	{
		public int StatementID { get; set; }

    [DataType(DataType.Date)]
    public DateTime StatementDate { get; set; }
		public virtual ICollection<AccountBalance> AccountBalances { get; set; }
	}
}
