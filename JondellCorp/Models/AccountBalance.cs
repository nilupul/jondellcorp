using System;
namespace JondellCorp.Models
{
	public class AccountBalance
	{
		public int AccountBalanceID { get; set; }
		public string Name { get; set; }
		public double Balance { get; set; }
		public int StatementID { get; set; }
	}
}
