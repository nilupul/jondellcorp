using JondellCorp.Models;
using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;

namespace JondellCorp.Data
{
  public static class DbInitializer
  {
    public static void Initialize(AccountsContext context)
    {
      context.Database.EnsureCreated();

      // Look for any students.
      if (context.Statements.Any())
      {
        return;   // DB has been seeded
      }

      List<Statement> statements = new List<Statement>();
      statements.Add(new Statement { StatementDate = DateTime.ParseExact("January-2017", "MMMM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None) });
      statements.Add(new Statement { StatementDate = DateTime.ParseExact("February-2018", "MMMM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None)});

      foreach (Statement statement in statements)
      {
        context.Statements.Add(statement);
      }
      context.SaveChanges();
    }
  }
}
