import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

export const routes: Routes = [
    {
        path: 'view-balances',
        loadChildren: './viewbalance/viewbalance.module#ViewBalanceModule'
    },

    {
        path: 'update-balances',
        loadChildren: './updatebalance/updatebalance.module#UpdateBalanceModule'
    },

    {
        path: 'reports',
        loadChildren: './reports/reports.module#ReportsModule'
    },

    {
        path: '', redirectTo: 'view-balances', pathMatch: 'full'
    },
    {
        path: '**', redirectTo: 'view-balances'
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
