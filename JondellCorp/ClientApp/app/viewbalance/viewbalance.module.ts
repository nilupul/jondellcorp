import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewBalanceComponent } from './viewbalance.component';
import { Http } from '@angular/http';
import { ViewBalanceRoutingModule } from "./viewbalance-routing.module";
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ViewBalanceRoutingModule,
    FormsModule
  ],
  declarations: [ViewBalanceComponent]
})
export class ViewBalanceModule { }
