import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ViewBalanceComponent} from "./viewbalance.component";

const routes: Routes = [
  {
    path: '',
    component: ViewBalanceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewBalanceRoutingModule { }

