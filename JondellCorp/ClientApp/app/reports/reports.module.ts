import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { Http } from '@angular/http';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import {ReportsRoutingModule} from "./reports-routing.module";

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    ChartsModule
  ],
  declarations: [ReportsComponent]
})
export class ReportsModule { }
