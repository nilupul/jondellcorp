# Jondell Corp App

Contoso University demonstrates how to use Entity Framework Core in an
ASP.NET Core MVC web application.

## Prerequisites
    Node.js 
    .NET Core 
    Angular CLI Installed globally

## Development Environment Setup

Open the solution, JondellCorp.sln in Visual Studio 2017

Run `npm install` to install application’s dependencies

Run `ng build` to build the Angular application

### Run
Run `dotnet run` to run application in watch mode

Run `ng serve --proxy-config proxy.config.json` to run angular application in watch mode

### File Watch

Run `dotnet watch run` to run application in watch mode

Run `ng serve --proxy-config proxy.config.json` to run angular application in watch mode

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Azure deployment

Change configuration in Visual Studio to `Release`

Now Publish the app from Visual studio to Azure